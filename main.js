var isInTiradas = false;
var isInUser = false;
var jugada_valida = false;
var turno =true;
var us;
var d =  new Date();
var fecha = d.getDate()+"/"+d.getMonth()+1+"/"+d.getFullYear();
var ganador;
var db;
var historial = "";
function distributeCards(id,addevent,t){
    var cont = 0;
    function reparte (){
        cont++;
        var padre =  document.getElementById('mazo_cards');
        var carta_repartida = padre.lastElementChild.lastElementChild; //imagen de la carta
        var id_carta_repartida = carta_repartida.getAttribute("id");    
        padre.removeChild(padre.lastElementChild); //Removemos el div contenedora de la
        var card_cont = document.createElement("div");
        var new_card = document.createElement("img");
        new_card.setAttribute("id",id_carta_repartida);
        new_card.setAttribute("class","apear");
        new_card.setAttribute("src","imgs/"+id_carta_repartida+".png");
        card_cont.appendChild(new_card);
        if(addevent){
            card_cont.addEventListener("dragstart",arrastraAtiradas,false);
            card_cont.addEventListener("dragend",eliminadeUser,false);
        }
        document.getElementById(id).appendChild(card_cont);
        if (cont===t) 
            clearInterval(inervalo);      
    }
    var inervalo = setInterval(reparte,1000);  
}


function mixCards(){
    var cartas =  new Array(52);
    for(var i=0; i<cartas.length; i++) 
        cartas[i]=i+1;
    
    cartas.sort(function() {
        return .5 - Math.random();
    });

    var padre = document.getElementById('mazo_cards');
    cartas.forEach(element => {
        var carta_conteainer  = document.createElement('div');
        var carta  = document.createElement('img');
        carta.setAttribute("src","imgs/"+element+".png");
        carta.setAttribute("id",element);
        carta_conteainer.appendChild(carta);
        padre.appendChild(carta_conteainer);              
    });
}

function estaEn(id,r1,r2){
    if(id>=r1 && id<=r2)
        return true;
    else 
        return false;
}

function BuscaPorValor(id,a,ind,rol){
    var band = false;
    var card = null;
    for (var i = 0; i < a.length; i++) {
        if(i!=ind){
            if(id==a[i]){
                band =true;
                card=a[i];
                break;
            }        
        }else
            continue;
    }
    if(rol=="user")
        return band;
    else
        return card;
}

function jaugadaValida_porvalor(id,id_card,rol){
    var status;
    // id's de las cartas con los mismos valres
    juego_valores =[ 
     [1,14,27,40],
     [2,15,28,41],
     [3,16,29,42],
     [4,17,30,43],
     [5,18,31,44],
     [6,19,32,45],
     [7,20,33,46],
     [8,21,34,47],
     [9,22,35,48],
     [10,23,36,49],
     [11,24,37,50],
     [12,25,38,51],
     [13,26,39,52]];
    
     for(var i=0; i<juego_valores.length; i++){
        //  rescatams el conjunto de valores para los cuelaes el valor de la carta es el mismo
         var ind = juego_valores[i].indexOf(id_card); 
         if(ind!=-1){
            status = BuscaPorValor(id,juego_valores[i],ind,rol);
            break;
         }
     }

     return status;
}


function jaugadaValida_porfigura(id,id_card){
    var band=false;
    if(id_card>=1 && id_card <=13){        //Corazones       
        band = estaEn(id,1,13);
    }else if(id_card >=14 && id_card <=26){ //Picas
        band = estaEn(id,14,26);
    }else if(id_card>=27 && id_card <= 39){//Diamantes
        band = estaEn(id,27,39);
    }else{                                  //Tréboles
        band = estaEn(id,40,52);
    }
    return band;

}

function arrastraAtiradas(e){
    var carta = e.target; //carta o imagen
    e.dataTransfer.setData("Text",carta.getAttribute("id"));
}

function eliminadeUser(e){
    var message;
    var padre = document.getElementById('user');
    var card = e.target;
    if(jugada_valida && isInTiradas){
        padre.removeChild(card.parentNode);
        isInTiradas=false;
        var padre_mensaje = document.getElementById('mensajes');
        if(thereiscard('user')){
            message = "!Felicidades has ganado!";
            padre_mensaje.style.background="rgb(67, 216, 67)";
            removeEventsMazo();
            removeEventsUser();
            add(us,fecha,us); 
        }else{
            message = "Turno de la PC";
            // padre_mensaje.style.background="rgb(19, 99, 95)";
            pc();
        }
        padre_mensaje.style.margin="10px";
        padre_mensaje.removeChild(padre_mensaje.firstElementChild);
        var frase = document.createElement('h3');
        var contenido = document.createTextNode(message);
        frase.appendChild(contenido);
        padre_mensaje.appendChild(frase);
        // padre_mensaje.style.background="rgb(15, 72, 179)";   
    }
        
}

function thereiscard(id){
    if(document.getElementById(id).childElementCount==0) return true;
    else return false;   
}

// Evetnos de de un jugaa la arrastrar la carta al area de juego
function dragandDropEventsUser(){
    var destino = document.getElementById('tiradas');
     // Cuando la carta entre en la zona de cartas tiradas
     var elems_origen = document.getElementById('tiradas').children;
     //Agregamos eventos Drag_drop a las cartas
    for(var i=0; i<elems_origen.length; i++){
        elems_origen[i].addEventListener("dragstart",arrastraAtiradas,false);
        elems_origen[i].addEventListener("dragend",eliminadeUser,false);
    }
     
     destino.addEventListener("dragenter",function(e){        
        e.preventDefault();
        // Recuperamos el id de la carta que entrao que ademas es la que se quiere compartir
        var id = e.dataTransfer.getData("Text"); 
        isInTiradas=true; //La carta a soltar está dentro del area de juegos
        var origin = (document.getElementById(id).parentNode.parentNode).getAttribute("id");
        console.log(document.getElementById(id));
        if(origin == "user"){
            if(thereiscard('tiradas')){
                jugada_valida=true;
                destino.style.background="#cbf078 ";
                destino.style.border="dashed 5px green";
            }else{       
                // Recuperamos el id de la carta del area de juegos
                var id_card = parseInt(document.getElementById('tiradas').firstElementChild.getAttribute("id")); 
                // validamos las jugadas
                if(jaugadaValida_porfigura(id,id_card)||jaugadaValida_porvalor(id,id_card,'user')){
                    jugada_valida=true;
                    destino.style.background="#cbf078 ";
                    destino.style.border="dashed 5px green";
                }else{
                    destino.style.background="#f97272";
                    destino.style.border="dashed 5px red";
                    jugada_valida=false; 
                }
            } 
           
        }
      
    },false);
    // Al retirar la carta de la jugada
    destino.addEventListener("dragleave",function(e){
        e.preventDefault();
        destino.style.background="#237a23";
        destino.style.border="radius 5px";                        
        destino.style.border="dashed 3px transparent";
        isInTiradas=false;
    },false);

    // Al momento de soltar la carta al area de juego
    destino.addEventListener("drop",function(e){
        e.preventDefault();
        var id = e.dataTransfer.getData("Text");
        var origen = (document.getElementById(id).parentNode.parentNode).getAttribute("id");        
        var src = document.getElementById(id).src;
        if(jugada_valida){               
            var padre = document.getElementById('tiradas');
            if(thereiscard('tiradas')!=true)
                padre.removeChild(padre.firstElementChild);                                      
            var carta_tirada = document.createElement("img");
            var card_cont = document.createElement("div");
            carta_tirada.setAttribute("src",src);
            card_cont.setAttribute("id",id);
            card_cont.setAttribute("class","soltar");
            card_cont.appendChild(carta_tirada);
            padre.appendChild(card_cont);            
        }
        destino.style.background="#237a23";
        destino.style.border="radius 5px";                        
        destino.style.border="dashed 3px transparent";
        
    },false);

    destino.addEventListener("dragover",function(e){
        e.preventDefault();
    },false);
}

function cardEventMazo(){
    var padre = document.getElementById('mazo_cards');
    var new_card = padre.lastElementChild;
    new_card.removeEventListener("click",cardEventMazo);
    new_card.addEventListener("dragstart",arrastraAtiradas,false);
    new_card.addEventListener("dragend",eliminadeUser,false);
    new_card.firstElementChild.setAttribute("class","apear");
    document.getElementById('user').appendChild(new_card);
    if(!thereiscard('pc') && !thereiscard('user') && thereiscard('mazo_cards')){
        var padre = document.getElementById('controles');
         var frase = document.createElement('h2');
         var content = document.createElement('div');
         var contenido = document.createTextNode("!Empate!");
         frase.appendChild(contenido);
         content.appendChild(frase);
         padre.appendChild(content);
         padre.style.removeProperty;
         padre.style.background="rgb(206, 193, 16)";
         padre_mensaje.style.margin="10px";
         removeEventsUser();
         removeEventsMazo();
    }
}

function addEventCardsMazo(){
    var elems_origen = document.getElementById('mazo_cards').children;    
    for(var i=0; i<elems_origen.length; i++){
        elems_origen[i].addEventListener("click",cardEventMazo,false);
        elems_origen[i].firstElementChild.setAttribute("class","take");
    }
}

function removeEventsMazo(){
    var elems_origen = document.getElementById('mazo_cards').children;    
    for(var i=0; i<elems_origen.length; i++){
        elems_origen[i].firstElementChild.removeAttribute("class");        
        elems_origen[i].removeEventListener("click",cardEventMazo,false);           
    }
}

function removeEventsUser(){
    var elems_origen = document.getElementById('user').children;    
    for(var i=0; i<elems_origen.length; i++){
        elems_origen[i].removeEventListener("dragstart",arrastraAtiradas,false);           
        elems_origen[i].removeEventListener("dragend",eliminadeUser,false);  
    }                
}

function hayJugada(){
     // Cartas en mano
     var cardhand = new Array();
     var cartaspc = document.getElementById('pc').children;
     var carta_seleccionada = null;
     for(var i=0; i<cartaspc.length; i++)
         cardhand.push(parseInt(cartaspc[i].firstElementChild.getAttribute("id")));
         console.log(cardhand);
     //Carta en el area de juego    
     var carta_en_area = parseInt((document.getElementById('tiradas').firstElementChild).getAttribute("id"));
    //  Jugada por figura
     for(var i=0; i<cardhand.length; i++){
         if(jaugadaValida_porfigura(cardhand[i],carta_en_area)){
             carta_seleccionada = cardhand[i];
             break;
         }
     }
    //  Jugada por valor
    if(carta_seleccionada == null){
        for(var i=0; i<cardhand.length; i++){
            if(jaugadaValida_porvalor(cardhand[i],carta_en_area,'pc')!=null){
                carta_seleccionada=cardhand[i];
            }
        }
    }
   
    return carta_seleccionada;
}

function pc(){
   var messge;
   var carta_selected = hayJugada();
   var padre_mensaje = document.getElementById('mensajes');
   if(carta_selected!=null){
    setTimeout(function(){
        var carta_en_mano = document.getElementById(carta_selected);
        carta_en_mano.setAttribute("class","desapear");
        setTimeout(function(){
            var padre = document.getElementById('tiradas');
            var car = document.getElementById(carta_selected);
            padre.removeChild(padre.firstElementChild);
            var card = document.createElement('img');
            var card_cont = document.createElement('div');
            card_cont.setAttribute("id",carta_selected); 
            card.setAttribute("src","imgs/"+carta_selected+".png");
            card_cont.setAttribute("class","soltar");
            card_cont.appendChild(card);
            document.getElementById('tiradas').appendChild(card_cont);
            document.getElementById('pc').removeChild(car.parentNode);

            if(thereiscard('pc')){
                messge="!Has perdido!";
                padre.style.removeProperty;         
                padre_mensaje.style.background="rgb(240, 84, 57)";
                removeEventsUser();
                removeEventsMazo();
                add(us,fecha,"PC"); 
               }else{
                   messge ="Es tu turno "+us;
               }
                padre_mensaje.removeChild(padre_mensaje.firstElementChild);
                var frase = document.createElement('h3'); 
                var contenido = document.createTextNode(messge);
                frase.appendChild(contenido);
                padre_mensaje.appendChild(frase);  
        },3000);
        
          
    },4000);
   }else{
        distributeCards('pc',false,1); 
       setTimeout(function(){
        pc();
       },3000);
   }
}

function play(){
    mixCards(); 
    distributeCards('user',true,3);
    setTimeout(function(){distributeCards('pc',false,3)},3000);
    dragandDropEventsUser();
    addEventCardsMazo();
    setTimeout(function(){
        var padre = document.getElementById('mensajes');
        var frase = document.createElement('h3');
        var contenido = document.createTextNode("Es tu turono "+us);
        frase.appendChild(contenido);
        padre.appendChild(frase);
    },7000);
}

function main (){
    us = prompt("¡Bienvenido!\nInserta un nombre de usuario");
    CreaBD();
    play();
    document.getElementById('again').addEventListener('click',function(){location.reload();});
    document.getElementById('finish').addEventListener('click',function(){
        removeEventsMazo();
        removeEventsUser();
        var padre = document.getElementById('mensajes');
        padre.removeChild(padre.firstElementChild);
        var frase = document.createElement('h3');
        var contenido = document.createTextNode("Juego finalizado");
        frase.appendChild(contenido);
        padre.appendChild(frase);
        padre.style.background="rgb(15, 72, 109)";
        padre.firstElementChild.style.color="#fff";
        add(us,fecha,"ninguno"); 
    });
    document.getElementById('hitorial').addEventListener('click',function(){
        readAll();
    });
}

function CreaBD(){
    var request = window.indexedDB.open("Partidas", 1);
    request.onerror = function (event) {
        console.log("error: ");
    };

    request.onsuccess = function (event) {
        db = request.result;
        console.log("success: " + db);
    };

    request.onupgradeneeded = function (event) {
        var bdd = event.target.result;
        var objectStore = bdd.createObjectStore("jugador", { keyPath: "id", autoIncrement:true });
    }
}
// Leer de la BDD
function readAll() {
    var objectStore = db.transaction("jugador").objectStore("jugador");
    // document.write("<table border='1'>");
    // document.write("<thead>");
    // document.write("<th>Partida</th>");
    // document.write("<th>Usuario</th>");
    // document.write("<th>Fecha</th>");
    // document.write("<th>Ganador</th>");    
    // document.write("</thead>");
    var histo = "";
    objectStore.openCursor().onsuccess = function (event) {
        var cursor = event.target.result;
        
        // document.write("<tr>");
        if (cursor) {
            // document.write("<td>"+cursor.key +"</td>");            
            // document.write("<td>"+cursor.value.usuario+"</td>");            
            // document.write("<td>"+ cursor.value.fecha+"</td>");            
            // document.write("<td>"+cursor.value.ganador+"</td>");            
            alert("Partida " + cursor.key + " Usuario " + cursor.value.usuario + ",Fecha: " + cursor.value.fecha + ", Ganador: " + cursor.value.ganador+"\n");
            cursor.continue();
        }
        // document.write("</tr>");
         
    };
    // document.write("</table>");
}
// Agregar a la BDD
function add(user,fech,winer) {
    var request = db.transaction(["jugador"], "readwrite")
        .objectStore("jugador")
        .add({usuario: user, fecha: fech, ganador: winer });

    request.onsuccess = function (event) {
        console.log(us+" has been added to your database.");
    };

    request.onerror = function (event) {
        // alert("Unable to add data\r\nKenny is aready exist in your database! ");
    }
}

window.addEventListener('load',main,false);